var {sequelize, camion, camionChofer, chofer, rutaCamion, ruta, rutaParada} = require('../models')
module.exports = {
  async indexCamionChofer (req, res) {
    try
    {
      const transportes = await camionChofer.findAll({ include: [{ all: true }]});
      res.send(transportes)
    }catch (err) {
      console.log(err)
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async postCamionChofer (req, res) {
    try {
      const [query] = (await sequelize.query('SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "camionChofer"'))[0]
      autoincrement= query.AUTO_INCREMENT      
      const camionchofer = await camionChofer.create({
        id:autoincrement,
        idCamion:req.body.idCamion,
        idChofer:req.body.idChofer
      })
      res.send(camionchofer)
    } catch (error) {
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async deleteCamionChofer (req, res) {
    try
    {
      await camionChofer.destroy({where: {id: req.params.id}})
      res.status(200).send(camion)
    }catch (err) {
      res.status(500).send({message: "true"})
    }
  },
  async indexRutaCamion (req, res) {
    try
    {
      const transportes = await rutaCamion.findAll({ include: [{ all: true }]});
      res.send(transportes)
    }catch (err) {
      console.log(err)
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async postRutaCamion (req, res) {
    try {
      const [query] = (await sequelize.query('SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "camionRuta"'))[0]
      autoincrement= query.AUTO_INCREMENT      
      const rutacamion = await rutaCamion.create({
        id:autoincrement,
        idRuta:req.body.idRuta,
        idCamion:req.body.idCamion
      })
      res.send(rutacamion)
    } catch (error) {
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async deleteRutaCamion (req, res) {
    try
    {
      await rutaCamion.destroy({where: {id: req.params.id}})
      res.status(200).send({message: "true"})
    }catch (err) {
      res.status(500).send(err)
    }
  },
  async indexRutaParada (req, res) {
    try
    {
      const transportes = await rutaParada.findAll({ include: [{ all: true }]});
      res.send(transportes)
    }catch (err) {
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async postRutaParada (req, res) {
    try {
      const [query] = (await sequelize.query('SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "rutaParada"'))[0]
      autoincrement= query.AUTO_INCREMENT      
      const camionruta = await rutaParada.create({
        id:autoincrement,
        idRuta:req.body.idRuta,
        idParada:req.body.idParada
      })
      res.send(camionruta)
    } catch (error) {
      res.status(500).send({error: 'An error has ocurred'})
    }
  },
  async deleteRutaParada (req, res) {
    try
    {
      await rutaParada.destroy({where: {id: req.params.id}})
      res.status(200).send({message: "true"})
    }catch (err) {
      res.status(500).send(err)
    }
  }
}