var express = require('express');
var router = express.Router();
const relationscontroller = require('../controllers/relationscontroller')
router.route('/camionchofer')
  .get(relationscontroller.indexCamionChofer)
  .post(relationscontroller.postCamionChofer)

router.route('/camionchofer/:id')
  .delete(relationscontroller.deleteCamionChofer)

router.route('/rutacamion')
  .get(relationscontroller.indexRutaCamion)
  .post(relationscontroller.postRutaCamion)

router.route('/rutacamion/:id')
  .delete(relationscontroller.deleteRutaCamion)

router.route('/rutaparada')
  .get(relationscontroller.indexRutaParada)
  .post(relationscontroller.postRutaParada)

router.route('/rutaparada/:id')
  .delete(relationscontroller.deleteRutaParada)
module.exports = router;
